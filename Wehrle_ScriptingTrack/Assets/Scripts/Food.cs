﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food
{
    
    public static int foodQuality = Random.Range(1,11);
    public Food()
    {
        //Debug.Log("Food Constructor");

    }

    public void Eat()
    {
        Debug.Log("Food has been eaten.");        
    }
    public void HowWasIt(){
        
        Debug.Log("The food was "+foodQuality.ToString()+"/10 quality.");
        foodQuality = Random.Range(1,11);
    }
    public void Smell()
    {
        Debug.Log("The Food smells ok.");
    }
    public virtual void LookAt()
    {
        Debug.Log("The Food looks fine.");
    }

}