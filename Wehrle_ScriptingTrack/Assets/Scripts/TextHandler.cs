﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextHandler : MonoBehaviour
{

    public Text WoodText;
    public Text StoneText;
    public Text FoodText;
    public Text HousesText;
    public Text TotalPeopleText;
    public Text UnemployedText;
    public Text HomelessText;
    public Text InstructionsText;
    public Text SpaceText;
    public Text ListOfPeopleText;
    public Text DayText;
    public Text RIPText;
    public GameObject ScriptObj;
    public string killname;
    People p;

    //using properties instead of public variables.
    int dayCount;
    public int DayCounter{get{return dayCount;}set{dayCount = value;}}
    int woodCount;
    public int WoodCounter{get{return woodCount;}set{woodCount = value;}}
    int stoneCount;
    public int StoneCounter{get{return stoneCount;}set{stoneCount = value;}}
    int foodCount;
    public int FoodCounter{get{return foodCount;}set{foodCount = value;}}
    int houseCount;
    public int HouseCounter{get{return houseCount;}set{houseCount = value;}}
    int peopleCount;
    public int PeopleCounter{get{return peopleCount;}set{peopleCount = value;}}
    
    int unemployedCount;
    public int UnemployedCounter{get{return unemployedCount;}set{unemployedCount = value;}}
    int homelessCount;
    public int HomelessCounter{get{return homelessCount;}set{homelessCount = value;}}

    //boolean to tell if they need instruction or not.
    public bool needInstruction;
    void Awake(){
        p = ScriptObj.GetComponent<People>();

        InstructionsText.enabled = true;
        SpaceText.enabled = true;
        ListOfPeopleText.enabled = false;
        DayText.enabled = true;
        StoneText.enabled = true;
        WoodText.enabled = true;
        FoodText.enabled = true;
        HousesText.enabled = true;
        TotalPeopleText.enabled = true;
        UnemployedText.enabled = true;
        HomelessText.enabled = true;
        RIPText.enabled = true;
        needInstruction = false;
    }
    
    void LateUpdate() {
        DayText.text = "Day "+ dayCount;

        WoodText.text = "Wood: "+ woodCount;
        StoneText.text = "Stone: "+ stoneCount;
        FoodText.text = "Food: "+ foodCount;
        HousesText.text = "Houses : "+ houseCount;
        TotalPeopleText.text = "Total People: "+ peopleCount;
        UnemployedText.text = "Unemployed: "+ unemployedCount;
        HomelessText.text = "Homeless: " + homelessCount;

        if(needInstruction){
            InstructionsText.enabled = true;
        }

        ListOfPeopleText.text = p.List();
    }
    
    public IEnumerable DeathWait()
    {
        RIPText.text = killname + " just died. RIP.";
        //print("Do is happen");
        yield return new WaitForSeconds(1.5f);
        RIPText.text = "";
    }
    
}
