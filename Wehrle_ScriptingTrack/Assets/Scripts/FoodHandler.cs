﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class FoodHandler : MonoBehaviour
{
    void Start () 
    {
        //this class was just for testing polymorphism originally.
        //Food myFood = new Meat();

        //myFood.Eat();

        //Meat myMeat = (Meat)myFood;

        //myMeat.Eat();
    }
}
