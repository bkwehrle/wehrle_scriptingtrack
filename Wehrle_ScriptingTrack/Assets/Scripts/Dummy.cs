﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dummy : MonoBehaviour
{
    // Start is called before the first frame update


    /*
     *
     * 1 properties prop get/set          DONE
     * 2 ternary bool ? ifthing : elsething DONE
     * 3 quaternions DONE
     * 4 Lists DONE
     * 5 Dictionarys DONE
     * 6 Coroutines DONE
     * 7 Polymorphism DONE
     * 8 Method Overloading DONE
     * 9 Member Hiding DONE
     * 10 Overriding DONE
     * 11 Attributes DONE
     * 12 Static DONE
     * 
     * 
     * Extension Methods
     * Delegates
     * Attributes like [Range(-100, 100)] and [ExecuteInEditMode]
     * EventManager 
     * Generics 
     * Static 
     * Interface
     * Namespaces
     * 
     * 
     * Project ideas:
     * 
     * completely text based.
     * 
     * a village game where the player makes workers that do various things to keep the game going.
     * Heres how i imagine it looking.
     * 
     * Wood 3 
     * Stone 7
     * Food  12
     * Total People 10
     * Houses 7
     * Homeless 3
     * 
     * You can make a ((W)oodcutter, (M)iner, (F)armer, (H)ouse builder, or (C)heck jobs)
     * 
     * Debug.ClearDeveloperConsole()
     * 
     * 
     * 
     */

    // Update is called once per frame
    void Update()
    {
    }
}
