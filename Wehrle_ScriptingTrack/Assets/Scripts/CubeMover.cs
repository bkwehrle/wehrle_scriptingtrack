﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMover : MonoBehaviour
{
    //attribute made to make sure my timer isnt going over 2
    [Range(0f, 2.5f)] float timer = 0.0f;
    
    bool spunOnce = false;
    public bool go = false;
    // Update is called once per frame
    void Update()
    {
        if (go)
        {
            transform.rotation = Quaternion.Slerp(Quaternion.Euler(0, -90, 0), Quaternion.Euler(0, 90, 0), timer);
            timer += Time.deltaTime;
        }

    }
    public void Rotate180()
    {
        if (timer< 2f)
        {
            //chill out
        } else {
            timer = 0.0f;
        }
    }
    //method overriding. Here i made a Rotate180 method that can run int i times.
    public int Rotate180(int i)
    {
        int tmp = i;
        while (tmp < 0){
            if (timer< 2f)
            {
                //chill out
            } else {
                timer = 0.0f;
            }
            tmp -= 1;
        }
        return 0;
        
    }
}
