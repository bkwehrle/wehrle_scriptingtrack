﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class People : MonoBehaviour
{
    
    // people will have name (key) and a job (value)
    public Dictionary<string,string> people = new Dictionary<string,string>();
    List<string> names;
    List<string> jobs;
    int startingPeople = 8;
    int randomNameNum;
    int randomJobNum;
    public int woodCutterCount;
    public int minerCount;
    public int farmerCount;
    public int builderCount;

    public GameObject mytextHandlerObj;
    TextHandler mytextHandler;

    void Start(){
        //we want that good randomness
        Random.InitState(System.DateTime.Now.Millisecond);

        //this is so i can update the people values.
        mytextHandler = mytextHandlerObj.GetComponent<TextHandler>();

        woodCutterCount = 0;
        minerCount = 0;
        farmerCount = 0;
        builderCount = 0;
        //list of random names
        names = new List<string>{"Drew","Cole","Barrack","Hope","Albert","Oats","Brooke","Kate","Joey","Luke","Olivia","David","Scott","Jordan","Ian"};
        
        jobs = new List<string>{"Woodcutter","Miner","Farmer","Builder","Unemployed"};
        //what job or unemployed which i guess isnt a job but whatever, im not gonna name my variable "employmentStatus"

       
        //populating my dictionary
        for (int i = 0; i < startingPeople; i++){
            randomNameNum = Random.Range(0,names.Count);
            
            randomJobNum = Random.Range(0,jobs.Count);
            if (jobs[randomJobNum] == "Woodcutter"){
                woodCutterCount += 1;
            }
            else if (jobs[randomJobNum] == "Miner"){
                minerCount += 1;
            }
            else if (jobs[randomJobNum] == "Farmer"){
                farmerCount += 1;
            }
            else if (jobs[randomJobNum] == "Builder"){
                builderCount += 1;
            }
            else if (jobs[randomJobNum] == "Unemployed"){
                mytextHandler.UnemployedCounter += 1;
            }
            mytextHandler.PeopleCounter += 1;
            people.Add(names[randomNameNum],jobs[randomJobNum]);
            names.Remove(names[randomNameNum]);
        }
        Homeless();
    }
    public void AddPerson(){
        randomNameNum = Random.Range(0,people.Keys.Count);
        
        randomJobNum = Random.Range(0,jobs.Count);
        people.Add(names[randomNameNum],jobs[randomJobNum]);
    }
    public void KillRandomPerson(){
        List<string> keys = new List<string>(people.Keys);
        //getting a list of all the names
        randomNameNum = Random.Range(0,keys.Count);
        //get a random number to pick a random name


        //have to minus them from the job count
        if (people[keys[randomNameNum]] == "Woodcutter")
        {
            woodCutterCount -= 1;
        }
        else if (people[keys[randomNameNum]] == "Miner")
        {
            minerCount -= 1;
        }
        else if (people[keys[randomNameNum]] == "Farmer")
        {
            farmerCount -= 1;
        }
        else if (people[keys[randomNameNum]] == "Builder")
        {
            builderCount -= 1;
        }
        else if (people[keys[randomNameNum]] == "Unemployed")
        {
            mytextHandler.UnemployedCounter -= 1;
        }   
        mytextHandler.PeopleCounter -= 1;
        people.Remove(keys[randomNameNum]);
        mytextHandler.killname = keys[randomNameNum];
        mytextHandler.StartCoroutine("DeathWait");
        //print(people.Count);
    }
    string chosenOne;
    public void AssignJob(string job){
        bool breakout = true;
        if (job != null){  
            for (int i = 0; i < 4;i++){
                if (job == jobs[i]){
                    List<string> keys = new List<string>(people.Keys);
                    //print(keys[7]);
                    //print("keys of index 7");
                    int j = 0;
                    while (j < keys.Count && breakout){
                        if (people[keys[j]] == "Unemployed"){
                            //print("someone got a job!");
                            chosenOne = keys[j];
                            if (jobs[i] == "Woodcutter"){
                                woodCutterCount += 1;
                            }
                            else if (jobs[i] == "Miner"){
                                minerCount += 1;
                            }
                            else if (jobs[i] == "Farmer"){
                                farmerCount += 1;
                            }
                            else if (jobs[i] == "Builder"){
                                builderCount += 1;
                            }
                            mytextHandler.UnemployedCounter -= 1;
                            breakout = false;
                        } 
                        j +=1;
                        if (j == keys.Count){
                            breakout = false;
                        }
                    }
                    
                    people[chosenOne] = jobs[i];
                }
            }
        }
    }
    public string List(){
        return "You have " +woodCutterCount.ToString()+" Woodcutters, "+minerCount.ToString()+
        " Miners," +farmerCount.ToString()+" Farmers, and "+ builderCount.ToString()+" Builders.";
    }

    public void Homeless()
    {
        //2 people per house.
        int housesFilled = mytextHandler.HouseCounter * 2;
        int homeless = 0;
        for (int i = 0; i <mytextHandler.PeopleCounter; i++)
        {
            housesFilled -= 1;
            if (housesFilled < 0)
            {
                homeless += 1;
            }
        }
        mytextHandler.HomelessCounter = homeless;
        //print(homeless);
        //return homeless;
    }
}
