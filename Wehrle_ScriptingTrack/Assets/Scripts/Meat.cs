﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meat : Food 
{
    public Meat()
    {
        //print("meat constructor");
    }

    public new void Eat()
    {
        Debug.Log("Meat has been eaten.");       
    }

    //this is member hiding. the food version of Smell is hiden when i make this no one.
    new public void Smell()
    {
        Debug.Log("The meat smells amazing!");
    }

    //this is a method override. Any virtual methods in the parent class can be overridden.
    public override void LookAt()
    {
        Debug.Log("The Meat looks fine.");
    }
}
