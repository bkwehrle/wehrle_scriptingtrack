﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class EventHandler : MonoBehaviour
{
    // I originally just made this class to get my basic UI up and running.
    //

    public GameObject textHandlerObj;
    TextHandler textHandler;
    public GameObject CubeMoverObj;
    CubeMover cubemover;
    //public GameObject scriptObj;
    People people;
    bool runTheSim;
    //attribute made to make sure my timer isnt going over 1
    [Range(0f, 1.5f)] float timer;
    Food myFood;
    void Awake()
    {
        myFood = new Meat();
        
        //setting the day to day one.
        people = GetComponent<People>();
        textHandler = textHandlerObj.GetComponent<TextHandler>();
        cubemover = CubeMoverObj.GetComponent<CubeMover>();
        //public GameObject
        textHandler.DayCounter = 1;

        //giving some starting resources

        textHandler.WoodCounter = Random.Range(3,6);
        textHandler.StoneCounter = Random.Range(2, 4);
        textHandler.FoodCounter = Random.Range(22, 28);
        textHandler.HouseCounter = Random.Range(1, 3);
        textHandler.PeopleCounter = 0;
        textHandler.UnemployedCounter = 0;
        textHandler.HomelessCounter = 0;

        //to seperate days
        runTheSim = false;
    }

    // Update is called once per frame

    void Update()
    {
        timer += Time.deltaTime;
        if (!runTheSim)
        {

        
            textHandler.InstructionsText.enabled = true;
            if (textHandler.UnemployedCounter > 0)
            {

                if (Input.GetKeyDown("w"))
                {
                    //print("here w ");
                    people.AssignJob("Woodcutter");
                }
                else if (Input.GetKeyDown("m"))
                {
                    people.AssignJob("Miner");
                }
                else if (Input.GetKeyDown("f"))
                {
                    people.AssignJob("Farmer");
                }
                else if (Input.GetKeyDown("b"))
                {
                    people.AssignJob("Builder");
                }
            }
            if (Input.GetKeyDown("c"))
            {
                StartCoroutine(CheckJobsDelay());


            }
        }
        else
        {
            textHandler.InstructionsText.enabled = false;
        }

        if (Input.GetKeyDown("r"))
        {
            SceneManager.LoadScene(0);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            runTheSim = runTheSim ? false : true;
        }
        if (timer >= 1f)
        {
            if (runTheSim)
            {
                RunSim();
                
                timer = 0f;
            }
            
        }
    }
        
    public void RunSim(){
        cubemover.Rotate180();
        cubemover.go = true;
        int randomChance;
        //you build a house for every builder that has access to a stone and a wood.
        for (int i = 0; i < people.builderCount; i++)
        {
            if (textHandler.WoodCounter >= 1 && textHandler.StoneCounter >= 1)
            {
                textHandler.WoodCounter -= 1;
                textHandler.StoneCounter -= 1;
                textHandler.HouseCounter += 1;
            }
        }
        //for every farmer there is a 80% chance they will yield a food. (40% chance for 1 food, 40% for 2 food)
        for (int i = 0; i < people.farmerCount; i++)
        {
            randomChance = Random.Range(0, 10);
            if (randomChance > 1)
            {
                textHandler.FoodCounter += Random.Range(1,3);
                //print("food tho");
            }
        }
        //for every woodcutter there is a 90% chance they will yield a wood.
        for (int i = 0; i < people.woodCutterCount; i++)
        {
            randomChance = Random.Range(0, 10);
            if (randomChance > 0)
            {
                textHandler.WoodCounter += 1;
            }
        }
        //for every miner there is a 60% chance they will yield a wood.
        for (int i = 0; i < people.minerCount; i++)
        {
            randomChance = Random.Range(0, 10);
            if (randomChance > 3)
            {
                textHandler.StoneCounter += 1;
            }
        }
        //1 food feeds about 5 people per tick, if there isnt enough there is a one in 3 chance someone will die.
        for (int i = 0; i < textHandler.PeopleCounter; i++)
        {
            //20% chance someone eats food
            randomChance = Random.Range(0, 6);
            if (randomChance == 0)
            {
                myFood.Eat();
                myFood.HowWasIt();
                textHandler.FoodCounter -= 1;
            }
            if (textHandler.FoodCounter < 0)
            {
                randomChance = Random.Range(0, 3);
                if (randomChance == 0)
                {
                    people.KillRandomPerson();
                }
                textHandler.FoodCounter = 0;
            }
        }
        if (textHandler.FoodCounter < 0)
        {
            textHandler.FoodCounter = 0;
        }
        //on average, one in eight homeless people will die per simulation tick
        //print("Homeless:");
        //print(textHandler.HomelessCounter);
        people.Homeless();
        for (int i = 0; i < textHandler.HomelessCounter; i++)
        {
            //print(i);
            randomChance = Random.Range(0, 8);
            if (randomChance == 0)
            {
                people.KillRandomPerson();
            }
        }
        textHandler.DayCounter += 1;
    }
    IEnumerator CheckJobsDelay(){
        textHandler.ListOfPeopleText.enabled = true;
        yield return new WaitForSeconds(3.0f);
        textHandler.ListOfPeopleText.enabled = false;
    }
}
